# Authors

This tool has been mainly developed by:

- Jaime ARIAS (CNRS, LIPN, Université Sorbonne Paris Nord) - arias@lipn.univ-paris13.fr
- Ghassene BOUACHIR (LIPN, CNRS, Université Sorbonne Paris Nord) - bouachir@lipn.univ-paris13.fr
- Hadhami ELOUNI (LIPN, CNRS, Université Sorbonne Paris Nord) - hadhami.ouni@lipn.univ-paris13.fr

Under the scientific supervision:
- Mohamed Taha BENNANI
- Kaïs KLAI
- Hanen OCHI