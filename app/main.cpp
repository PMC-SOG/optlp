#include <CLI11.hpp>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "aggregate.hpp"
#include "graph.h"
#include "path_generator.h"
#include "regex_generator.h"
#include "utils.h"

/**
 * Print the statistics of the optimal paths
 * @param output_file - the output file
 * @param optimal_paths - the optimal paths
 * @param generated_paths - the generated paths
 * @param random_graphs - whether the input is random graphs
 */
void PrintStatistics(const std::filesystem::path &output_file,
                     const std::vector<std::string> &optimal_paths,
                     const std::vector<std::string> &generated_paths,
                     bool random_graphs) {
  std::stringstream log;
  std::ofstream file(output_file);

  if (!file) {
    std::cerr << "Error: File not found or could not be opened.\n";
    std::exit(EXIT_FAILURE);
  }

  if (random_graphs) {
    size_t avg_opt_size = 0;
    for (const std::string &path : optimal_paths) {
      avg_opt_size += path.size();
    }
    const size_t nb_paths = optimal_paths.size();
    log << "Optimal paths:\n";
    log << " # of generated paths: " << generated_paths.size() << "\n";
    log << " # of optimal paths: " << nb_paths << "\n";
    log << " Average optimal paths size: " << avg_opt_size / nb_paths << '\n';
  } else {
    int index = 0;
    size_t avg_opt_size = 0;
    for (const std::string &path : optimal_paths) {
      index++;
      avg_opt_size += path.size();

      log << "Optimal path (" << index << "):" << "\n  - path: " << path
          << "\n  - length: " << std::count(path.begin(), path.end(), '_')
          << "\n\n";
    }

    log << "# of observed paths: " << index << "";
    log << "\nAverage optimal paths size: " << avg_opt_size << "\n";
  }

  // print to both stdout and file
  std::cout << log.str();
  file << log.str();
  file.close();
}

/**
 * Run the pipeline
 * @param lts - the lts graph
 * @param output_filename - the output file
 * @param random_graphs - whether the input is random graphs
 */
void RunPipeline(Graph *lts, const std::filesystem::path &output_filename,
                 const bool random_graphs) {
  const float start_time = GetTime();

  // generate the regex
  std::cout << "\nGenerating regex from LTS ... ";
  const Lg languages = RegexGenerator::Generate(lts);

  // get the language of the regex
  std::cout << "\n Language of the regex: "
            << RegexGenerator::Language(languages);

  const float expr_time = GetTime() - start_time;
  std::cout << "\n Elapsed time for regex: " << expr_time << " seconds";

  // vector of generated paths
  std::vector<std::string> generated_paths;
  std::cout << "\n\nGenerating paths ... ";
  for (const LgElement &e : languages) {
    for (const std::string &path : PathGenerator::ExtractPaths(e.first)) {
      if (!path.empty()) {
        generated_paths.push_back(path);
      }
    }
  }

  const float paths_time = GetTime() - expr_time;
  std::cout << "\n Elapsed time for generating paths: " << paths_time
            << " seconds";
  std::cout << "\n # of no null paths: " << generated_paths.size();

  size_t average_size = 0;
  for (const std::string &path : generated_paths) {
    average_size += std::count(path.begin(), path.end(), '_');
  }
  average_size = average_size / generated_paths.size();
  std::cout << "\n Average size of generated paths: " << average_size;

  // extract the shortest paths
  std::cout << "\n\nExtracting shortest paths ...\n";
  const std::vector<std::string> optimal_paths =
      PathGenerator::ExtractShortestPaths(lts->labels(), generated_paths);

  // check if the problem does not have an optimal solution.
  if (optimal_paths.empty()) {
    return;
  }

  const float elapsed_time = GetTime() - start_time;

  std::cout << "\nOptimal paths found\n===================\n";
  PrintStatistics(output_filename, optimal_paths, generated_paths,
                  random_graphs);
  std::cout << "\nTotal time: " << elapsed_time << " seconds\n";
}

int main(const int argc, char *argv[]) {
  // TEST: using sogMBT files
  Aggregate *new_aggregate = new Aggregate;
  new_aggregate->visited = true;
  std::cout << "sogMBT: " << new_aggregate->visited << std::endl;

  CLI::App app{
      "OptLP: A tool to find the minimum number of shortest paths "
      "that cover all system actions"};

  bool random_graphs_used = false;
  auto *random_graphs = app.add_subcommand("random", "Generate a random LTS");
  random_graphs->callback([&]() { random_graphs_used = true; });

  int nb_nodes = 0;
  random_graphs->add_option("--nb-nodes", nb_nodes, "Number of nodes")
      ->required();

  int nb_edges = 0;
  random_graphs->add_option("--nb-edges", nb_edges, "Number of edges")
      ->required();

  int nb_labels = 0;
  random_graphs->add_option("--nb-labels", nb_labels, "Number of labels")
      ->required();

  auto *from_file =
      app.add_subcommand("from-file", "Load an LTS from a dot file");

  std::filesystem::path input_file;
  from_file->add_option("--input-file", input_file, ".dot file")
      ->type_name("Path")
      ->required()
      ->check(CLI::ExistingFile);

  std::filesystem::path output_folder = "./";
  from_file
      ->add_option("--output-folder", output_folder,
                   "output folder [default: ./]")
      ->type_name("Path")
      ->check(CLI::ExistingDirectory);

  // parse arguments
  CLI11_PARSE(app, argc, argv);

  Graph *lts = nullptr;
  std::string filename;
  if (*random_graphs) {
    filename = "random";
    lts = Graph::GenerateRandomGraph(nb_nodes, nb_edges, nb_labels);
    SaveToFile(output_folder / "random.dot", lts->ToString());
  } else if (*from_file) {
    filename = input_file.stem().string();
    lts = new Graph(input_file);
  } else {
    std::cout << app.help();
    return 1;
  }

  const std::filesystem::path output_filename =
      "result_opt_" + filename + ".txt";
  RunPipeline(lts, output_folder / output_filename, random_graphs_used);

  return 0;
}
