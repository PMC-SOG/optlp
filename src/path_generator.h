#ifndef PATH_GENERATOR_H_
#define PATH_GENERATOR_H_

#include <set>
#include <string>
#include <vector>

typedef struct Element {
  std::string expression;
  bool star;
  std::vector<std::string> result = {""};
} Element;

class PathGenerator {
 public:
  /**
   * Split a regex into a vector of subregex by searching the outer * symbol
   * @param regex the string to split
   * @return the vector of strings
   */
  static std::vector<Element> SplitExpression(const std::string &regex);

  // TODO(Jaime): review this
  static std::vector<std::string> ConstructWord(
      const std::vector<Element> &regex_l);

  // TODO(Jaime): review this
  static std::vector<std::string> ExtractPaths(const std::string &regex);

  /**
   * Extract the shortest paths from the generated paths using integer linear
   * programming
   * @param labels - the set of labels
   * @param generated_paths - the set of generated paths
   * @return the shortest paths
   **/
  static std::vector<std::string> ExtractShortestPaths(
      const std::set<std::string> &labels,
      const std::vector<std::string> &generated_paths);
};

#endif  // PATH_GENERATOR_H_
