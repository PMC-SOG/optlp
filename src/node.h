#ifndef NODE_H_
#define NODE_H_

#include <string>
#include <vector>

class Node;

typedef std::string Label;
typedef std::pair<Label, Node *> Transition;

class Node {
 private:
  /** node's identifier */
  int id_;

  /** node's name */
  std::string name_;

  /** list of node's successors */
  std::vector<Transition> successors_;

 public:
  /**
   * Create a new node
   * @param id - the id of the node
   */
  explicit Node(int id);

  /**
   * Create a new node with a name
   * @param id - the id of the node
   * @param name - the name of the node
   */
  Node(int id, const std::string &name);

  /**
   * Delete the node
   */
  virtual ~Node();

  /**
   * Get the id of the node
   * @return the id of the node
   */
  int id() const;

  /**
   * Get the name of the node
   * @return the name of the node
   */
  std::string name() const;

  /**
   * Set the id of the node
   * @param id - the id of the node
   */
  void set_id(int id);

  /**
   * Set the name of the node
   * @param name - the name of the node
   */
  void set_name(const std::string &name);

  /**
   * Get the successors of the node
   * @return the successors of the node
   */
  std::vector<Transition> &successors();

  /**
   * Set the successors of the node
   * @param successors - the new successors of the node
   */
  void set_successors(const std::vector<Transition> &successors);
};

#endif  // NODE_H_
