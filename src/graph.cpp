#include "graph.h"

#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "utils.h"

using namespace std;

/**
 * Generate a random number between 0 and max
 * @param max - the maximum value
 * @return the random number
 */
int random(const int max) {
  return static_cast<int>(arc4random() % max);
}

/**
 * Generate a random string of a given length
 * @param length - the length of the string
 * @return the random string
 */
string RandomString(const int length) {
  std::string result;
  constexpr int alphabet_size = 26;

  constexpr std::array<char, alphabet_size> alphabet = {
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
      'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

  for (int i = 0; i < length; ++i) {
    result += alphabet.at(random(alphabet_size));
  }

  return result;
}

/**
 * Get a random element from a set
 * @param elements set of elements
 * @return the random element
 */
template <typename T>
T GetRandomElement(const std::set<T> &elements) {
  return *std::next(elements.begin(), random(elements.size()));
}

Graph::Graph() = default;

Graph::Graph(Node *n) : initial_node_(n) {}

Graph::~Graph() = default;

Node *Graph::initial_node() const {
  return initial_node_;
}

void Graph::set_initial_node(Node *n) {
  initial_node_ = n;
}

vector<Node *> Graph::states() {
  return states_;
}

void Graph::set_states(const vector<Node *> &states) {
  states_ = states;
}

Node *Graph::GetState(const int id) const {
  return states_[id];
}

void Graph::AddState(const int id, const std::string &name) {
  const auto n = new Node(id, name);
  states_.push_back(n);
}

void Graph::AddLabel(const std::string &label) {
  labels_.insert(label);
}

std::set<std::string> Graph::labels() {
  return labels_;
}

void Graph::AddSuccessor(const int source, const int target,
                         const std::string &label) const {
  const Transition t = {label, GetState(target)};
  GetState(source)->successors().push_back(t);
}

Graph::Graph(const string &filename) {
  ifstream file(filename);  // open the file

  // Check if the file is open, throw an exception if not
  if (!file.is_open()) {
    throw std::runtime_error("Error: Could not open file.");
  }

  string line;
  map<string, int> map_nodes;  // map for name and index in the states vector

  // regex matching the labeled directed edge (e.g., A -> B [label="1"];)
  std::regex tr_regex(R"((\w+)\s*->\s*(\w+)\s*\[\s*label\s*=\s*\"(.*?)\"\];?)");

  // regex matching the initial node (e.g., start -> A;)
  std::regex initial_regex(R"(start\s*->\s*(\w+);?)");

  // read line by line
  string initial_state;
  while (getline(file, line)) {
    // Check if the line is a transition
    if (std::smatch match; std::regex_search(line, match, tr_regex)) {
      std::string source = match[1].str();       // source node
      std::string target = match[2].str();       // target node
      std::string label = match[3].str() + "_";  // transition label

      int source_id = std::stoi(Split(source, '_')[1]);
      int target_id = std::stoi(Split(target, '_')[1]);

      // source state does not exist
      if (map_nodes.find(source) == map_nodes.end()) {
        this->AddState(source_id, source);
        map_nodes.insert({source, states().size() - 1});
      }

      // target state does not exist
      if (map_nodes.find(target) == map_nodes.end()) {
        this->AddState(target_id, target);
        map_nodes.insert({target, states_.size() - 1});
      }

      // label does not exist
      if (const set<std::string> labels = this->labels();
          labels.find(label) == labels.end()) {
        this->AddLabel(label);
      }

      this->AddSuccessor(map_nodes[source], map_nodes[target], label);
    }
    // check if line is the initial node
    else if (std::regex_search(line, match, initial_regex)) {
      initial_state = match[1].str();
    }
  }

  if (initial_state.empty()) {
    throw std::runtime_error("Error: Initial state not found.");
  }

  this->set_initial_node(states_[map_nodes[initial_state]]);

  // Close the file
  file.close();
}

Graph *Graph::GenerateRandomGraph(const int n_states, const int n_transitions,
                                  const int n_labels) {
  auto *graph = new Graph();

  // create states
  for (int i = 0; i < n_states; i++) {
    graph->AddState(i, "s_" + std::to_string(i));
  }

  // generate a set of labels
  std::set<std::string> labels_pool;
  while (labels_pool.size() < n_labels) {
    labels_pool.insert(RandomString(2) + "_");
  }

  // set initial state
  graph->set_initial_node(graph->GetState(0));

  // set of nodes that have already successors
  std::set<int> connected_nodes;
  connected_nodes.insert(0);

  // set of nodes that are not yet connected
  std::set<int> unconnected_nodes;
  for (int i = 1; i < n_states; ++i) {
    unconnected_nodes.insert(i);
  }

  // track used labels for each node to avoid duplicates
  std::map<Node *, std::set<std::string>> used_labels;

  int counter_transitions = 0;  // number of transitions generated so far
  while (!unconnected_nodes.empty() && counter_transitions < n_transitions) {
    // randomly choose an already connected state as the source
    const int source_id = GetRandomElement(connected_nodes);
    Node *source = graph->GetState(source_id);

    // randomly choose an unconnected state as the target
    int target_id = GetRandomElement(unconnected_nodes);

    // Remove the target node from the unconnected set, and
    // add it to the connected set
    unconnected_nodes.erase(target_id);
    connected_nodes.insert(target_id);

    // Ensure the chosen transition label is unique for the current node
    std::string label;
    do {
      label = GetRandomElement(labels_pool);
    } while (used_labels[source].count(label) > 0);

    // Mark the transition as used for the current node
    used_labels[source].insert(label);

    // Add the transition
    graph->AddSuccessor(source_id, target_id, label);
    graph->AddLabel(label);

    counter_transitions++;
  }

  // add additional transitions randomly
  while (counter_transitions < n_transitions) {
    // choose a random source node
    const int source_id = random(n_states);
    Node *source = graph->GetState(source_id);

    // Ensure the chosen transition label is unique for the current node
    std::string label;
    do {
      label = GetRandomElement(labels_pool);
    } while (used_labels[source].count(label) > 0);

    // Mark the transition label as used for the current node
    used_labels[source].insert(label);

    // Choose a random target node
    const int target_id = random(n_states);

    // Add the transition
    graph->AddSuccessor(source_id, target_id, label);
    graph->AddLabel(label);
    counter_transitions++;
  }

  // Print graph statistics
  std::cout << "Graph statistics: \n";
  std::cout << " # nodes: " << graph->states().size()
            << " | # transitions: " << counter_transitions
            << " | # labels: " << graph->labels().size() << '\n';

  return graph;
}

std::string Graph::ToString() {
  std::stringstream ss;

  ss << "digraph g {\n";
  ss << "  start [shape=point, width=0];\n\n";
  ss << "  start -> " << this->initial_node()->name() << ";";

  for (Node *n : this->states()) {
    for (const auto &v : n->successors()) {
      // remove the trailing _ from labels
      string label = v.first;
      label.pop_back();

      ss << "\n  " << n->name() << " -> " << v.second->name() << " [label = \""
         << label << "\"];";
    }
  }
  ss << "\n}";
  return ss.str();
}
