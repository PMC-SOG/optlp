#include "node.h"

using namespace std;

Node::Node(const int id) : id_(id) {}

Node::Node(const int id, const string &name) : id_(id), name_(name) {}

Node::~Node() = default;

int Node::id() const {
  return id_;
}

string Node::name() const {
  return name_;
}

void Node::set_id(const int id) {
  id_ = id;
}

void Node::set_name(const string &name) {
  name_ = name;
}

vector<Transition> &Node::successors() {
  return successors_;
}

void Node::set_successors(const vector<Transition> &successors) {
  successors_ = successors;
}
