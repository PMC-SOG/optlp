#include "path_generator.h"

#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <unordered_map>

#include "ortools/linear_solver/linear_solver.h"
#include "utils.h"

using namespace std;

vector<Element> PathGenerator::SplitExpression(const string &regex) {
  vector<char> stack;
  string str;
  int begin = 0;
  vector<Element> expression;

  for (int i = 0; i < regex.size(); i++) {
    // start an expression between parenthesis
    if (char a = regex[i]; a == '(') {
      // it's not a ( inner parenthesis
      if (stack.empty()) {
        begin = i + 1;

        if (!str.empty()) {
          Element e;
          e.star = false;
          e.expression = str;
          expression.push_back(e);
        }
        str = "";
      }

      stack.push_back(a);
    }
    // end of expression between parenthesis
    else if (a == ')') {
      stack.pop_back();

      // it's not a ) inner parenthesis
      if (stack.empty()) {
        Element e;
        e.star = true;
        e.expression = regex.substr(begin, i - begin);
        expression.push_back(e);

        // skip * symbol
        // TODO(Jaime): maybe it can be better
        i++;
      }
    }
    // it's an expression between parenthesis
    else if (stack.empty()) {
      str.push_back(a);
    }
  }

  if (!str.empty()) {
    Element e;
    e.star = false;
    e.expression = str;
    expression.push_back(e);
  }

  return expression;
}

vector<string> PathGenerator::ConstructWord(const vector<Element> &regex_l) {
  vector<string> words;
  for (const Element &e : regex_l) {
    // if it's not an right-side expression of an * operator
    if (!e.star) {
      if (words.empty()) {
        words.push_back(e.expression);
      } else {
        for (string &w : words) {
          w.append(e.expression);
        }
      }
    } else {
      const size_t n_words = words.size();
      const size_t res_size = e.result.size();

      // duplicate the prefix of the new word
      // TODO(any): why from i =1 ?
      vector<string> copy = words;
      for (int i = 1; i < res_size; i++) {
        words.insert(end(words), begin(copy), end(copy));
      }

      for (int i = 0; i < res_size; i++) {
        string new_word = e.result[i];
        // if there is no prefix word
        if (n_words == 0) {
          words.push_back(new_word);
        } else {
          // update only a copy
          for (size_t j = n_words * i; j < n_words * (i + 1); j++) {
            words[j].append(new_word);
          }
        }
      }
    }
  }

  return words;
}

vector<string> PathGenerator::ExtractPaths(const string &regex) {
  vector<Element> regex_l = SplitExpression(regex);

  for (Element &e : regex_l) {
    if (e.star) {
      // TODO(Jaime): this can be done better
      vector<string> v = ExtractPaths(e.expression);

      // remove empty paths
      if (v[0].empty()) {
        e.result.pop_back();
      }

      e.result.insert(end(e.result), begin(v), end(v));
    }
  }

  return ConstructWord(regex_l);
}

std::vector<std::string> PathGenerator::ExtractShortestPaths(
    const std::set<std::string> &labels,
    const std::vector<std::string> &generated_paths) {
  const size_t rows = labels.size();
  const size_t columns = generated_paths.size();

  // Precompute the index of each label in the vector
  std::unordered_map<std::string, int> labels_index;
  int index = 0;
  for (const std::string &l : labels) {
    labels_index[l] = index;
    index++;
  }
  if (index != rows) {
    throw std::runtime_error("# of labels is different from # of rows");
  }

  cout << " Constraint matrix size: (rows: " << rows << ", columns: " << columns
       << ")\n";

  const float start_time = GetTime();

  // initialisation of the constraint matrix
  cout << " Initialising the constraint matrix ... ";
  std::vector<std::vector<int>> constraints(rows, std::vector<int>(columns, 0));

  // Charge constraint matrix with paths from regex
  for (int path_index = 0; path_index < columns; path_index++) {
    std::vector<std::string> paths = Split(generated_paths[path_index], '_');
    for (std::string &l : paths) {
      constraints[labels_index[l + "_"]][path_index] = 1;
    }
  }
  cout << "DONE\n";

  // Create the MP solver with the SCIP backend.
  const std::unique_ptr<operations_research::MPSolver> solver(
      operations_research::MPSolver::CreateSolver("SCIP"));
  if (!solver) {
    throw std::runtime_error("SCIP solver unavailable");
  }

  // Create variables
  cout << " Creating variables ... ";
  std::vector<const operations_research::MPVariable *> x(columns + rows);
  for (int j = 0; j < columns; ++j) {
    x[j] = solver->MakeIntVar(0.0, 1, "x" + std::to_string(j));
  }
  cout << "DONE\n";

  // Create constraints
  cout << " Creating constraints ... ";
  for (const auto &l : labels) {
    operations_research::MPConstraint *constraint = solver->MakeRowConstraint(
        1, operations_research::MPSolver::infinity(), "");
    for (int j = 0; j < columns; ++j) {
      if (constraints[labels_index[l]][j] == 1) {
        constraint->SetCoefficient(x[j], 1);
      }
    }
  }
  cout << "DONE\n";

  // Create the objective function
  cout << " Creating objective function ... ";
  operations_research::MPObjective *const objective =
      solver->MutableObjective();
  for (int j = 0; j < columns; ++j) {
    // Set the coefficient for the objective function to the length of the path
    std::string path = generated_paths[j];
    const int path_size =
        static_cast<int>(std::count(path.begin(), path.end(), '_'));
    objective->SetCoefficient(x[j], path_size);
  }
  cout << "DONE\n";

  // Set the objective function to minimization
  objective->SetMinimization();

  // Find a solution
  const operations_research::MPSolver::ResultStatus result_status =
      solver->Solve();

  const float lp_time = GetTime() - start_time;
  cout << " Elapsed time for solving ILP problem: " << lp_time << " seconds\n";

  // Check that the problem has an optimal solution.
  std::vector<std::string> optimal_paths;
  if (result_status != operations_research::MPSolver::OPTIMAL) {
    std::cout << "The problem does not have an optimal solution.\n";
  } else {
    std::cout << " Solution => ";
    std::cout << "optimal objective value = " << objective->Value() << '\n';

    for (int j = 0; j < columns; ++j) {
      if (x[j]->solution_value() == 1) {
        optimal_paths.push_back(generated_paths[j]);
      }
    }
  }

  return optimal_paths;
}
