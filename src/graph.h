#ifndef GRAPH_H_
#define GRAPH_H_

#include <set>
#include <string>
#include <vector>

#include "node.h"

class Graph {
private:
  /** initial node of the graph */
  Node *initial_node_ = nullptr;

  /** list of states of the graph */
  std::vector<Node *> states_;

  /** list of labels of the graph */
  std::set<std::string> labels_;

public:
  /**
   * Create a new graph
   */
  Graph();

  /**
   * Create a new graph with an initial node
   * @param n - pointer to initial node of the graph
   */
  explicit Graph(Node *n);

  /**
   * Create a new graph from a dot file
   * @param filename - path to the file
   */
  explicit Graph(const std::string &filename);

  /**
   * Delete the graph
   */
  virtual ~Graph();

  /**
   * Get the initial node of the graph
   * @return pointer to initial node of the graph
   */
  Node *initial_node() const;

  /**
   * Set the initial node of the graph
   * @param n - pointer to initial node of the graph
   */
  void set_initial_node(Node *n);

  /**
   * Get the list of states of the graph
   * @return list of states of the graph
   */
  std::vector<Node *> states();

  /**
   * Add a state to the graph
   * @param id - id of the state
   * @param name - name of the state
   */
  void AddState(int id, const std::string &name);

  /**
   * Get a state from the graph
   * @param id - id of the state
   * @return pointer to the state
   */
  Node *GetState(int id) const;

  /**
   * Set the list of states of the graph
   * @param states - list of states of the graph
   */
  void set_states(const std::vector<Node *> &states);

  /**
   * Get the list of labels of the graph
   * @return list of labels of the graph
   */
  std::set<std::string> labels();

  /**
   * Add a label to the graph
   * @param label - label of the graph
   */
  void AddLabel(const std::string &label);

  /**
   * Add a successor to a node of the graph
   * @param source - source of the transition
   * @param target - target of the transition
   * @param label - label of the transition
   */
  void AddSuccessor(int source, int target, const std::string &label) const;

  /**
   * Generate a random graph
   * @param n_states - number of states
   * @param n_transitions - number of transitions
   * @param n_labels - maximum number of labels
   */
  static Graph *GenerateRandomGraph(int n_states, int n_transitions,
                                    int n_labels);

  /**
   * Return the string representation of the graph
   * @return string representation of the graph
   */
  std::string ToString();
};

#endif  // GRAPH_H_
