
#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <vector>

/**
 * Get the current time
 * @return the current time
 */
float GetTime();

/**
 * Split a string into a vector of strings using a delimiter
 * @param str - the string to split
 * @param delimiter - the delimiter
 * @return the vector of strings
 */
std::vector<std::string> Split(const std::string& str, char delimiter);

/**
 * Save a string to a file
 * @param filename - the name of the file
 * @param content - the content to save
 */
void SaveToFile(const std::string& filename, const std::string& content);

#endif  // UTILS_H_
