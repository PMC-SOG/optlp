#include "utils.h"

#include <ctime>
#include <fstream>
#include <sstream>

float GetTime() {
  return static_cast<float>(clock()) / static_cast<float>(CLOCKS_PER_SEC);
}

std::vector<std::string> Split(const std::string &str, const char delimiter) {
  std::vector<std::string> tokens;
  std::stringstream ss(str);
  std::string token;

  while (std::getline(ss, token, delimiter)) {
    tokens.push_back(token);
  }

  return tokens;
}

void SaveToFile(const std::string &filename, const std::string &content) {
  std::ofstream file;
  file.open(filename);
  file << content;
  file.close();
}
