
#ifndef REGEX_GENERATOR_H_
#define REGEX_GENERATOR_H_

#include <string>
#include <vector>

#include "graph.h"
#include "node.h"

typedef std::pair<std::string, Node *> LgElement;
typedef std::vector<LgElement> Lg;

class RegexGenerator {
 public:
  /**
   * Generate the regex from the graph
   * @param g the graph
   * @return the regex
   */
  static Lg Generate(const Graph *g);

  /**
   * Generate the language from the regex
   * @param lg the regex
   * @return the language
   */
  static std::string Language(const Lg &lg);
};

#endif  // REGEX_GENERATOR_H_
