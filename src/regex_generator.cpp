#include "regex_generator.h"

#include <algorithm>
#include <cstddef>
#include <map>
#include <set>

using namespace std;

/**
 * Concatenate a sequence with the sequence of an language
 * @param seq - the sequence
 * @param lg - the language
 * @param[out] updated_lg - updated language
 */
void ConcatSequences(const std::string &seq, const Lg &lg, Lg &updated_lg) {
  for (const LgElement &e : lg) {
    updated_lg.push_back({seq + e.first, e.second});
  }
}

/**
 * Join the sequence of an language
 * @param lg - the language
 * @return the updated language
 */
std::string JoinSequencesOfLg(const Lg &lg) {
  std::string seq;
  for (const LgElement &f : lg) {
    seq += "(" + f.first + ")*";
  }
  return "(" + seq + ")*";
}

/**
 * Update the language of a state
 * @param s - the state
 * @param s0 - the initial state
 * @param lg_map - the map of languages
 * @param visited - the set of visited states
 */
void UpdateLanguage(const Node *s, const int s0, std::map<int, Lg> &lg_map,
                    std::set<int> &visited) {
  if (visited.find(s->id()) != visited.end()) {
    Lg updated_lg = {};
    for (const LgElement &e : lg_map[s->id()]) {
      const std::string seq_e = e.first;

      if (const Node *state_e = e.second;
          state_e != nullptr && state_e->id() != s0) {
        UpdateLanguage(state_e, s0, lg_map, visited);
        ConcatSequences(seq_e, lg_map[state_e->id()], updated_lg);
      } else {
        updated_lg.push_back(e);
      }  // end if state_e  is not the initial state
    }  // end for each element of Lg(s)

    lg_map[s->id()] = updated_lg;
  }  // end if s is visited
}

/**
 * Generate the regular expression of a graph
 * @param st - a stack
 * @param lg_map - the map of languages
 * @param visited - the set of visited states
 * @param s0 - the initial state
 */
Lg RegularExpressionGeneration(std::vector<Node *> &st,
                               std::map<int, Lg> &lg_map,
                               std::set<int> &visited, const int s0) {
  Node *cur = nullptr;
  if (!st.empty()) {
    cur = st.back();

    // check if current state has no successors
    if (cur->successors().empty()) {
      lg_map[cur->id()] = {{"", nullptr}};
    } else {
      lg_map[cur->id()] = {};

      // process each successor of the current state
      for (const Transition &succ : cur->successors()) {
        Label label = succ.first;

        if (Node *next = succ.second;
            find(st.begin(), st.end(), next) != st.end()) {
          lg_map[cur->id()].push_back({label, next});
        } else {
          // check if is visited
          if (visited.find(next->id()) != visited.end()) {
            UpdateLanguage(next, s0, lg_map, visited);
          } else {
            st.push_back(next);
            RegularExpressionGeneration(st, lg_map, visited, s0);
          }  // end if next is visited

          // update language
          ConcatSequences(label, lg_map[next->id()], lg_map[cur->id()]);

        }  // end if next is in the stack
      }  // end for over successors

      // build cy and cy_compl sets
      Lg cy = {};
      Lg cy_compl = {};
      for (const LgElement &e : lg_map[cur->id()]) {
        if (e.second == cur) {
          cy.push_back(e);
        } else {
          cy_compl.push_back(e);
        }
      }

      if (!cy.empty()) {
        std::string seq = JoinSequencesOfLg(cy);
        if (!cy_compl.empty()) {
          Lg updated_lg = {};
          for (const LgElement &e : cy_compl) {
            updated_lg.push_back({seq + e.first, e.second});
          }
          lg_map[cur->id()] = updated_lg;
        } else {
          lg_map[cur->id()] = {{seq, nullptr}};
        }  // end if cy_compl is not empty
      }  // end if cy is not empty
    }  // end if current state has no successors
  }  // end if stack not empty

  cur = st.back();
  st.pop_back();

  // mark the current state as visited
  visited.insert(cur->id());

  /* std::cout << "return from " << cur->id() << std::endl; */
  /* for (const LgElement &e : lg_map[cur->id()]) { */
  /*   const std::string s = e.second == nullptr ? "_" : e.second->name(); */
  /*   std::cout << "  <" << e.first << ", " << s << ">" << std::endl; */
  /* } */

  return lg_map[cur->id()];
}

Lg RegexGenerator::Generate(const Graph *g) {
  std::vector<Node *> st;  // vector because we check the elements inside
  std::map<int, Lg> lg_map;
  std::set<int> visited;

  // Initialize the stack with the initial state
  st.push_back(g->initial_node());

  return RegularExpressionGeneration(st, lg_map, visited,
                                     g->initial_node()->id());
}

std::string RegexGenerator::Language(const Lg &lg) {
  std::string res;
  for (size_t i = 0; i < lg.size(); ++i) {
    if (i != 0) {
      res += " + ";
    }
    res += lg[i].first;
  }
  return res;
}
