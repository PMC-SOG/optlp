
#include "regex_generator.h"

#include <algorithm>
#include <catch2/catch_test_macros.hpp>

#include "graph.h"

template <typename T>
bool contains(const std::vector<T> &vec, const T &value) {
  return std::find(vec.begin(), vec.end(), value) != vec.end();
}

SCENARIO("it's possible to generate a regex from an LTS", "[regex]") {
  GIVEN("a dot file with a graph") {
    const std::string filename = "graph.dot";
    WHEN("the graph is created from the dot file") {
      Graph *g = new Graph(filename);
      AND_WHEN("the regex is generated") {
        Lg lg = RegexGenerator::Generate(g);
        THEN("the regex is correct") {
          REQUIRE(lg.size() == 3);

          REQUIRE(
              contains(lg, {"((a_b_c_)*(b_c_c_)*)*d_((f_a_e_)*)*", nullptr}));
          REQUIRE(contains(
              lg, {"((a_b_c_)*(b_c_c_)*)*b_d_e_e_((f_a_e_)*)*", nullptr}));
          REQUIRE(contains(lg, {"((a_b_c_)*(b_c_c_)*)*b_d_f_", nullptr}));
        }
      }
    }
  }
}
