#include "graph.h"

#include <algorithm>
#include <catch2/catch_test_macros.hpp>

#include "node.h"

SCENARIO("a graph has an initial node") {
  GIVEN("an empty graph") {
    Graph *g = new Graph();
    WHEN("the initial node is queried") {
      THEN("it returns null") {
        REQUIRE(g->initial_node() == nullptr);
      }
    }
  }

  GIVEN("a graph with an initial node") {
    Node *n = new Node(0);
    Graph *g = new Graph(n);
    WHEN("the initial node is queried") {
      THEN("it returns the initial node") {
        REQUIRE(g->initial_node()->id() == n->id());
      }
    }
  }
}

SCENARIO("a graph can be created from a dot file") {
  GIVEN("a dot file with a graph") {
    const std::string filename = "graph.dot";
    WHEN("the graph is created from the dot file") {
      Graph *g = new Graph(filename);
      THEN("the initial node is correct") {
        REQUIRE(g->initial_node()->id() == 0);
      }
      THEN("the number of nodes is correct") {
        std::vector<Node *> states = g->states();
        std::vector<std::string> state_names(states.size());
        std::transform(states.begin(), states.end(), state_names.begin(),
                       [](const Node *state) { return state->name(); });

        REQUIRE(states.size() == 9);
        REQUIRE(state_names == std::vector<std::string>(
                                   {"ag_0", "ag_1", "ag_2", "ag_3", "ag_4",
                                    "ag_5", "ag_6", "ag_7", "ag_8"}));
      }
      THEN("the number of labels is correct") {
        REQUIRE(g->labels().size() == 6);
      }
      THEN("the successors of the initial state are correct") {
        const std::vector<Transition> successors =
            g->initial_node()->successors();
        REQUIRE(successors.size() == 3);
        REQUIRE(successors[0].first == "d_");
        REQUIRE(successors[0].second->name() == "ag_1");
        REQUIRE(successors[1].first == "a_");
        REQUIRE(successors[1].second->name() == "ag_2");
        REQUIRE(successors[2].first == "b_");
        REQUIRE(successors[2].second->name() == "ag_3");
      }
    }
  }
}

SCENARIO("a graph can be created randomly") {
  GIVEN("a number of states, transitions, and labels") {
    const int num_states = 200;
    const int num_transitions = 100;
    const int num_labels = 20;
    WHEN("the graph is created randomly") {
      auto *g =
          Graph::GenerateRandomGraph(num_states, num_transitions, num_labels);

      THEN("the number of states is correct") {
        REQUIRE(g->states().size() == num_states);
      }

      THEN("the number of labels is correct") {
        REQUIRE(g->labels().size() <= num_labels);
      }
    }
  }
}
