
#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include "path_generator.h"

SCENARIO("a regex has paths") {
  GIVEN("a regex") {
    std::string regex = "((a_b_c_)∗(b_c_c_)∗)*b_d_f_";
    WHEN("it has * operators") {
      std::vector<Element> elements = PathGenerator::SplitExpression(regex);
      THEN("it is possible to decompose it in subregex") {
        REQUIRE(elements.size() == 2);

        REQUIRE(elements[0].result == std::vector<std::string>{""});
        REQUIRE(elements[0].expression == "(a_b_c_)∗(b_c_c_)∗");
        REQUIRE(elements[0].star == true);

        REQUIRE(elements[1].result == std::vector<std::string>{""});
        REQUIRE(elements[1].expression == "b_d_f_");
        REQUIRE(elements[1].star == false);
      }

      THEN("it is possible to generate words if the subregex has no *") {
        std::vector<Element> e{elements[1]};
        std::vector<std::string> words = PathGenerator::ConstructWord(e);

        REQUIRE(words.size() == 1);
        REQUIRE(words[0] == "b_d_f_");
      }

      THEN("it is possible to generate words if the subregex has *") {
        Element e1 = {"(a_b_c_)", true, {"", "a_b_c_"}};
        Element e2 = {"(b_c_c_)", true, {"", "b_c_c_"}};

        std::vector<Element> e{e1, e2};
        std::vector<std::string> words = PathGenerator::ConstructWord(e);

        REQUIRE(words.size() == 4);
        REQUIRE(words[0].empty());
        REQUIRE(words[1] == "a_b_c_");
        REQUIRE(words[2] == "b_c_c_");
        REQUIRE(words[3] == "a_b_c_b_c_c_");
      }
    }
  }
}
