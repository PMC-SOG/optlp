#include "node.h"

#include <catch2/catch_test_macros.hpp>

SCENARIO("a node has an id") {
  GIVEN("a node initialized with id 1") {
    Node *n = new Node(1);
    WHEN("the id is queried") {
      THEN("it returns 1") {
        REQUIRE(n->id() == 1);
      }
    }

    WHEN("the id is modified by 2") {
      n->set_id(2);
      THEN("it returns 2") {
        REQUIRE(n->id() == 2);
      }
    }
  }
}

SCENARIO("a node has a name") {
  GIVEN("a node initialized with name s0") {
    Node *n = new Node(0, "s0");
    WHEN("the name is queried") {
      THEN("it returns s0") {
        REQUIRE(n->name() == "s0");
      }
    }

    WHEN("the name is modified by s1") {
      n->set_name("s1");
      THEN("it returns s1") {
        REQUIRE(n->name() == "s1");
      }
    }
  }
}

SCENARIO("a node has successors") {
  GIVEN("an empty node") {
    Node *n = new Node(0);
    WHEN("the successors are queried") {
      THEN("it returns an empty vector") {
        REQUIRE(n->successors().size() == 0);
      }
    }
  }

  GIVEN("a node") {
    Node *n = new Node(0);
    WHEN("the successors are modified") {
      n->set_successors({{"a", new Node(1)}, {"b", new Node(2)}});
      THEN("it returns a vector with the right size") {
        REQUIRE(n->successors().size() == 2);
      }
      THEN("it returns a vector with the right elements") {
        REQUIRE(n->successors()[0].first == "a");
        REQUIRE(n->successors()[0].second->id() == 1);
        REQUIRE(n->successors()[1].first == "b");
        REQUIRE(n->successors()[1].second->id() == 2);
      }
    }
  }
}
